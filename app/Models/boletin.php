<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class boletin extends Model
{
    use HasFactory;
    protected $table="boletin";
    protected $primarykey = ["id"];
    public $timestamps = false;
    protected $fillable=['id', 'titulo', 'nom_busqueda', 'descripcion','tipo', 'ruta', 'fecha_boletin', 'usuario'];

}
