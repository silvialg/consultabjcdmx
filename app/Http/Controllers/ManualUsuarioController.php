<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ManualUsuarioController extends Controller
{
    public function index()
    {
        //genera una ruta completa a un archivo dado dentro del directorio public:
        $file = public_path('/ManualUsuario/ManualUsuarioPJCDMX.pdf'); 
        return response()->download($file);
    }
}
