<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
//use Illuminate\Http\Response;
//use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use App\Models\Otros\filtro;
use App\Models\boletin;
use App\Models\contador;
use App\Models\visitantes;
use App\Models\Post;
use App\Models\BitacoraAccion;
use Carbon\Carbon;

class ConsultaBoletinController extends Controller
{
    public function index()
    {
        //echo 'User IP Address : '. $_SERVER['REMOTE_ADDR'];
        //dd($ip = $_SERVER['REMOTE_ADDR']);
        //$clientIP = request()->ip();   
        //dd($clientIP);
        //$post = Post::find(1);
        //visits($post)->decrement();
        //echo $input = request()->all();
        //codigo para las visitas a la consulta del boletin   
        

        $ipaddress = '';
           if (isset($_SERVER['HTTP_CLIENT_IP']))
              $ipaddress = $_SERVER['HTTP_CLIENT_IP'];
           else if(isset($_SERVER['HTTP_X_FORWARDED_FOR']))
              $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
           else if(isset($_SERVER['HTTP_X_FORWARDED']))
              $ipaddress = $_SERVER['HTTP_X_FORWARDED'];
           else if(isset($_SERVER['HTTP_FORWARDED_FOR']))
               $ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
           else if(isset($_SERVER['HTTP_FORWARDED']))
               $ipaddress = $_SERVER['HTTP_FORWARDED'];
           else if(isset($_SERVER['REMOTE_ADDR']))
               $ipaddress = $_SERVER['REMOTE_ADDR'];
           else
               $ipaddress = 'UNKNOWN';    



     
         $fecha_dia = date("Y-m-d");
         $hora_dia = date("H:i:s");
         $ip = $_SERVER["REMOTE_ADDR"] ?? ""; 
        
        $contadorv = contador::where('fecha_consulta', '=', $fecha_dia)                   
        ->count();      

        if ($contadorv == null){           
            $visitas = new contador();
            $visitas->fecha_consulta =  $fecha_dia;    
            $visitas->contador =  1;                       
            $visitas->created_at = Carbon::now();
            $visitas->save();             
        }
        else { 
            DB::table('contadores')
            ->where('fecha_consulta', '=', $fecha_dia) 
           ->update(array('contador' =>DB::raw('contador + 1')));                   
        }
       
        //codigo visitantes a la consulta  
        //$nombreRuta = Request::route()->getName();      
        //$visitas = new visitantes();
        //$visitas->fecha_consulta =  $fecha_dia;
        //$visitas->hora_consulta =   $hora_dia;     
        //$visitas->ip = $ip;  
        //$visitas->pagina = 2; 
        //$visitas->id_empleado = auth()->user()->id; 
        //$visitas->pagina = $nombreRuta;                       
        //$visitas->created_at = Carbon::now();
        //$visitas->save();        

        //$nombreRuta = Route::currentRouteName();
        //echo($nombreRuta);
        //$payload = json_decode(file_get_contents("php://input"));
        //echo   $payload;
       
      

       // $sentencia = $bd->prepare("INSERT INTO visitas(fecha, ip, pagina, url) VALUES(?, ?, ?, ?)");
       // return $sentencia->execute([$fecha, $ip, $pagina, $url]);
        $contador =  1;         
        $fechaboletin = new \DateTime();
        $fechaboletin = $fechaboletin->format('Y-m-d');
        $fechafinal = new \DateTime();
        $fechafinal = $fechafinal->format('Y-m-d');
        $fechaini = Carbon::now();       
        $fechainicial = $fechaini->subDays(7, 'day');
        $fechainicial = $fechainicial->format('Y-m-d');
        //$fecha = Carbonfecha::now()->add(1, 'day');
        //Carbon::now()->subDays(30);     
        $boletines = boletin::where('iestatus', '=', 1)
         ->where('tipo','=', 2)
         ->where('fecha_boletin','>=', $fechainicial)
         ->where('fecha_boletin','<=', $fechafinal)
         ->orderby('fecha_boletin', 'DESC')
         ->get();      
        
         
        $visitas = new visitantes();
        $visitas->fecha_consulta =  $fecha_dia;
        $visitas->hora_consulta =   $hora_dia;     
        $visitas->ip = $ipaddress;  
        $visitas->pagina = 2; 
        $visitas->id_empleado = auth()->user()->id;                                 
        $visitas->created_at = Carbon::now();
        $visitas->save();      
         

        return view('consulta.consultaboletinj',compact('boletines','fechafinal','fechainicial','fechaboletin','contador'));      
                //return view('admin.admboletin');    
    }

    public function filtrar(Request $request)
    {
        //echo 'User IP Address : '. $_SERVER['REMOTE_ADDR'];
        //dd($ip = $_SERVER['REMOTE_ADDR']);
        //codigo para las visitas a la consulta del boletin              
        $fecha_dia = date("Y-m-d");
        $hora_dia = date("H:i:s");
       
        $ipaddress = '';
           if (isset($_SERVER['HTTP_CLIENT_IP']))
              $ipaddress = $_SERVER['HTTP_CLIENT_IP'];
           else if(isset($_SERVER['HTTP_X_FORWARDED_FOR']))
              $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
           else if(isset($_SERVER['HTTP_X_FORWARDED']))
              $ipaddress = $_SERVER['HTTP_X_FORWARDED'];
           else if(isset($_SERVER['HTTP_FORWARDED_FOR']))
               $ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
           else if(isset($_SERVER['HTTP_FORWARDED']))
               $ipaddress = $_SERVER['HTTP_FORWARDED'];
           else if(isset($_SERVER['REMOTE_ADDR']))
               $ipaddress = $_SERVER['REMOTE_ADDR'];
           else
               $ipaddress = 'UNKNOWN';    
       
        $ip = $_SERVER["REMOTE_ADDR"] ?? ""; 
        
        //codigo para las visitas a la consulta del boletin        
        DB::table('contadores')
        ->where('fecha_consulta', '=', $fecha_dia) 
        ->update(array('contador' =>DB::raw('contador + 1')));                   
        
        $fechaboletin = new \DateTime();
        $fechaboletin = $fechaboletin->format('Y-m-d');
        $fechafinal = $request->fechafinal;
        $fechainicial = $request->fechainicial;  
        $boletines = boletin::where('iestatus', '=', 1)
        ->where('tipo','=', 2)
        ->where('fecha_boletin','>=', $fechainicial)
        ->where('fecha_boletin','<=', $fechafinal)
        ->orderby('fecha_boletin', 'DESC')
        ->get();   


        $visitas = new visitantes();
        $visitas->fecha_consulta =  $fecha_dia;
        $visitas->hora_consulta =   $hora_dia;     
        $visitas->ip =  $ipaddress;  
        $visitas->pagina = 2;  
        $visitas->id_empleado = auth()->user()->id;                                
        $visitas->created_at = Carbon::now();
        $visitas->save();             
        
        return view('consulta.consultaboletinj',compact('boletines','fechafinal','fechainicial','fechaboletin'));   
            
    }


    public function descargar($id, Request $request)
    {
        $fecha_dia = date("Y-m-d");  
        $boletin = boletin::find($id); 
        $file  =  $boletin->titulo;
        //echo $pathtoFile = public_path().'/pdf/boletines/'.$file;
        // $pathtoFile = '/pdf/boletines/'.$file;
      
        //return view('interno',compact('boletin')); 
     
       
    }

    public function descargarboletin($id, Request $request)
    {
        $ipaddress = '';
           if (isset($_SERVER['HTTP_CLIENT_IP']))
              $ipaddress = $_SERVER['HTTP_CLIENT_IP'];
           else if(isset($_SERVER['HTTP_X_FORWARDED_FOR']))
              $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
           else if(isset($_SERVER['HTTP_X_FORWARDED']))
              $ipaddress = $_SERVER['HTTP_X_FORWARDED'];
           else if(isset($_SERVER['HTTP_FORWARDED_FOR']))
               $ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
           else if(isset($_SERVER['HTTP_FORWARDED']))
               $ipaddress = $_SERVER['HTTP_FORWARDED'];
           else if(isset($_SERVER['REMOTE_ADDR']))
               $ipaddress = $_SERVER['REMOTE_ADDR'];
           else
               $ipaddress = 'UNKNOWN';    
       
        $ip = $_SERVER["REMOTE_ADDR"] ?? ""; 
        $boletin = boletin::find($id); 
        $file  =  $boletin->titulo;
        $file = public_path('/pdf/boletines/'. $file);
        
        $visitas = new visitantes();
        $fecha_dia = date("Y-m-d");
        $hora_dia = date("H:i:s");
        $visitas->fecha_consulta =  $fecha_dia;
        $visitas->hora_consulta =   $hora_dia;     
        $visitas->ip =  $ipaddress;  
        $visitas->pagina = $boletin->titulo;  
        $visitas->id_empleado = auth()->user()->id;                                
        $visitas->created_at = Carbon::now();
        $visitas->save();     

        //return response()->file($file);
       
        return response()->download($file);
    }
  
    public function downfile($id)
    {
        
  
     $file = public_path('\pdf\boletines'. $ruta); 
    //return response()->file($file);
    return response()->download($file);
     
    // return response()->download(public_path('020220221.pdf'));
   
 
   
   // return response()->download(storage_path('pdf\boletines' . $ruta . ''));

  // return response()->download(storage_path('/pdf/boletines/' . $nombre . '.pdf'));
    //return response()->download($file_path2);


       // return response()->download(storage_path('/pdf/boletines/' . $nombre . '.pdf'));
        //echo $Datos = boletin::find($id);   
       // return response()->download(public_path('020220221.pdf'));
    }


}
