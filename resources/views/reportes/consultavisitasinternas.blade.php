@extends('layouts.app') 
@section('title','Consulta de visitas por usuario interno del PJCDMX')
@section('content') 
@include('msj')
<h3 class="text-center">Consulta de visitas por usuario del Poder Judicial de la Ciudad de México </h3> 
<br>
  <br>
  <form class="form-group" method="POST" action="{{ url('/visitasinternas/filtrar') }}" id="">          
       {{ csrf_field() }}

       <div class="form-group row justify-content-md-center">
          <div class="col-2">
              {!! Form::label('lblFechaInicial', 'Fecha Inicial :  ', array('for' => 'txtFechaInicial','class'=>'col-form-label')) !!}    
              {!! Form::date('fechainicial', $fechainicial); !!}
              
          </div>    
          <div class="col-2">
              {!! Form::label('lblFechaFinal', 'Fecha Final : ', array('for' => 'txtFechaFinal','class'=>'col-form-label')) !!}    
              {!! Form::date('fechafinal', $fechafinal); !!}
          </div> 
        </div>  
        <br>
        <div class="row justify-content-md-center">           
              <div class="col-1">
                 <button type="submit" class="btn btn-success"><span><i class="fa fa-filter" aria-hidden="true"></i></span> Filtrar</button>
              </div>               
        </div>
        <br><br><br>

        <div class="card">
        <div class="card-body">        
            <div class="form-group row justify-content-md-center">               
                <table class="table table-bordered" id="MyTable">
                    <thead>
                        <tr style="border: 1px solid"> 
                            <th class="text-center" >Empleado</th>  
                            <th class="text-center" >fecha</th> 
                            <th class="text-center" >ip</th>  
                            <th class="text-center" >visitas</th>                                                                                    
                        </tr>
                    </thead>  

                    <tbody>
                       
                        @foreach($visi_int as $visita)                          
                        <tr>                         
                                <td class="text-center" >{{$visita->name}}</td>                        
                                <td class="text-center" >{{$visita->fecha_consulta}}</td>
                                <td class="text-center" >{{$visita->ip}}</td>
                                <td class="text-center" >{{$visita->con_visitas}} </td>                                                                                            
                            </tr>                                                                                        
                        @endforeach
                    </tbody> 
                    <tfoot>
                        <tr style="border: 1px solid"> 
                        <th class="text-center" >Empleado</th>  
                            <th class="text-center" >fecha</th> 
                            <th class="text-center" >ip</th>  
                            <th class="text-center" >visitas</th>                              
                        </tr>
                    </tfoot>        
                </table>
            </div>   
        </div>
        </div>



</form>   
@endsection
