<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Http;
use App\Models\Otros\filtro;
use App\Models\boletin;
use App\Models\BitacoraAccion;
use Carbon\Carbon;
use GuzzleHttp\Client;

class AdmBoletinController extends Controller
{
   /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //echo 'User IP Address : '. $_SERVER['REMOTE_ADDR'];
        //dd($ip = $_SERVER['REMOTE_ADDR']);

        $fechaboletin = new \DateTime();
        $fechaboletin = $fechaboletin->format('Y-m-d');
        $fechafinal = new \DateTime();
        $fechafinal = $fechafinal->format('Y-m-d');
        $fechaini = Carbon::now();       
        $fechainicial = $fechaini->subDays(7, 'day');
        $fechainicial = $fechainicial->format('Y-m-d');
        //$fecha = Carbonfecha::now()->add(1, 'day');
        //Carbon::now()->subDays(30)     
        $boletines = boletin::where('iestatus', '=', 1)
         ->where('fecha_boletin','>=', $fechainicial)
         ->where('fecha_boletin','<=', $fechafinal)
         ->orderBy('fecha_boletin','desc')
         ->get();               
        return view('admin.admboletin',compact('boletines','fechafinal','fechainicial','fechaboletin'));    
        //return view('admin.admboletin');    
    }

    public function filtrar(Request $request)
    {
        //echo 'User IP Address : '. $_SERVER['REMOTE_ADDR'];
        //dd($ip = $_SERVER['REMOTE_ADDR']);

        $fechaboletin = new \DateTime();
        $fechaboletin = $fechaboletin->format('Y-m-d');
        $fechafinal = $request->fechafinal;
        $fechainicial = $request->fechainicial;  
        $boletines = boletin::where('iestatus', '=', 1)
        ->where('fecha_boletin','>=', $fechainicial)
        ->where('fecha_boletin','<=', $fechafinal)
        ->orderBy('fecha_boletin','desc')
        ->get();               
        
        return view('admin.admboletin',compact('boletines','fechafinal','fechainicial','fechaboletin'));    
        //dd($fechainicial);
      
       
        //$fechafinal = $fechafinalf->format('Y-m-d');
                    
        //$fechainicial = $fechainif->format('Y-m-d');
        
        //return view('admin.admboletin');    
    }




    public function subirboletin(Request $request)
    {    
        $fecha = new \DateTime();
        $nowOnlyHora = new \DateTime();
        $fecha = $fecha->format('Y-m-d');
        $file_ary = array(); 
        

        $file=$request->file("urlpdf");   
          
        if($request->hasFile("urlpdf")){
            $nombre_archivo = $request->file('urlpdf')->getClientOriginalName();
            //$nuevo_nombre1 =   Str::random(10).'.pdf';    
            $nuevo_nombre =   Str::random(10);    
            //$nombre = "pdf_".time().".".$file->guessExtension();    
            $nombre = "pdf_".$nuevo_nombre.".".$file->guessExtension();
            //$ruta = public_path("pdf/boletines/".$nombre_archivo);
            $ruta = public_path("pdf/boletines/".$nombre);
            
            if($file->guessExtension()=="pdf"){ //si el archivo es pdf              
                $buscaboletin = boletin::select('id','fecha_boletin')
                                         ->where('iestatus', '=', 1)
                                         ->where('fecha_boletin','=',$request->fechaboletin) 
                                         ->where('tipo','=',$request->tipob)
                                         ->first();

                
                if ($buscaboletin!=null) {
                   // dd($buscaboletin);   
                   return redirect()->route('boletin.index')->with('msjdelete','El boletín ya existe');                                            
                }else{
                    copy($file, $ruta);

                    $fecha_diab =  date("Y-m-d H:i:s");
                    $pathToFile = "pdf/boletines/".$nombre;   
                    $b64pdf = chunk_split(base64_encode(file_get_contents($pathToFile))); 
  
                   // $response = Http::post('http://172.19.40.132:8000/api/boletin', [
                    $response = Http::post('https://gestordocumental.poderjudicialcdmx.gob.mx/api/boletin', [
                        'metadata' => [
                            'FechaPublicacion' => $fecha_diab,
                            'NombreBoletin' => 'Boletin informativo 1',
                            'Area' => 'UTIC',
                            'QuienInforma' => 'Boletin Judicial',
                         ],
                        'filename' => $nombre,
                        'document' => $b64pdf,
                        ]);
                    $response->getStatusCode();
                    $response->getBody();
                    $response =json_decode($response); // Using this you can access any key like below
                    $idGlobal = $response->idGlobal; //access key  
                    $url = $response->url; 
                  
                    if ($response!=null) {
                       $boletin = new boletin();
                       $boletin->titulo =  $nombre;
                       $boletin->nom_busqueda = $nombre_archivo;
                       $boletin->descripcion = $idGlobal; 
                       $boletin->tipo = $request->tipob;
                       $boletin->ruta = $nombre;
                    
                       $boletin->ruta = $url;  
                    
                       $boletin->fecha_boletin = $request->fechaboletin;
                       $boletin->iestatus = 1;                        
                       $boletin->usuario = auth()->user()->id;
                       $boletin->created_at = Carbon::now();
                       $boletin->save();  
                    
                        //Llenamos la bitacora sube boletin
                       $ip = $_SERVER['REMOTE_ADDR'];
                       $bitacoraAcciones = new BitacoraAccion();
                       $bitacoraAcciones->id_usuario = auth()->user()->id;
                       $bitacoraAcciones->id_entidad = 1;          
                       $bitacoraAcciones->fecha = $fecha;
                       $bitacoraAcciones->hora = $nowOnlyHora;
                       $bitacoraAcciones->accion = "SUBIR BOLETIN";
                       $bitacoraAcciones->ip = $ip;
                       $bitacoraAcciones->id_general = " ";
                       $bitacoraAcciones->created_at = Carbon::now();
                       $bitacoraAcciones->save();    

                       return redirect()->route('boletin.index')->with('msjupdate','El boletín se subio correctamente');   
                
                    }else{
                        return redirect()->route('boletin.index')->with('msjdelete','No se puede subir el Bólenin intente más tarde');  
                    }
                }    

                //echo 'el archivo  es pdf';
                //dd($request->fechaboletin);
               
            }
            else
            {
                return redirect()->route('boletin.index')->with('msjdelete','El archivo no es pdf');
            }    
            
        }   
        
        else {
            return redirect()->route('boletin.index')->with('msjdelete','no se ha seleccionado el archivo');
        }
       
        //return redirect()->route('subirboletin')
        //->with('message','El archivo se firmo correctamente ...');
                          
    }


    // borrar boletin
    
    public function deleteBoletin($id, Request $request){
        $fecha = new \DateTime();
        $fecha = $fecha->format('Y-m-d');
        $nowOnlyHora = new \DateTime();
        $nowOnlyHora = $nowOnlyHora->format('H:i:s');
          //Llenamos la bitacora sube boletin
        $ip = $_SERVER['REMOTE_ADDR'];
        $Datos = boletin::find($id);
        $Datos->iestatus = 0;
        $Datos->save();   
        //Llenamos la bitacora que registra borrar del boletin         
        $ip = $_SERVER['REMOTE_ADDR'];        
        $bitacoraAcciones = new BitacoraAccion();
        $bitacoraAcciones->id_usuario = auth()->user()->id;   
        $bitacoraAcciones->id_entidad = 0;               
        $bitacoraAcciones->fecha = $fecha;
        $bitacoraAcciones->hora = $nowOnlyHora;
        $bitacoraAcciones->accion = "BORRAR BOLETIN";
        $bitacoraAcciones->ip = $ip;
        $bitacoraAcciones->id_general = $id;
        $bitacoraAcciones->updated_at = Carbon::now();
        $bitacoraAcciones->save();           
        return redirect()->route('boletin.index')->with('msjdelete','El boletin de fecha ( '. $Datos->fecha_boletin.') fue eliminada Correctamente.');
    }



}
