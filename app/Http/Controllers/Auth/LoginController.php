<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Redirect;
use App\Traits\VerificaempTrait;



class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;
    use VerificaempTrait; //FFV AGREGAMOS EL TRAIT

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    protected function credentials(Request $request){
        $idp = $request ->nempleado; 
        $login = $request->input($this->username());
        $rfc    = $request ->login ;
               
         // Get data using Trait method
        $valor = $this->getData($idp,$rfc);

        $field = filter_var($login,FILTER_VALIDATE_EMAIL) ? 'email' : 'rfc';

        
        if ($valor == '0')
         {  
            Session::flash('usuario_creado','EL USUARIO NO EXISTE FAVOR DE VERIFICAR');
            return[
                $field => $idp,
                'password' => $request->input('password')
             ];
            // return redirect()->route('register')
            //->with('msjdelete', 'EL EMPLEADO NO EXISTE, FAVOR DE VERIFICAR SUS DATOS');
         }else{

            return[
               $field => $login,
               'password' => $request->input('password')
            ];
         }
       
    }
    public function logout(Request $request) {
  
      Auth::logout();
      $request->session()->flush(); // limpio la session  'success', 'login_error','status', 'bitacoraID' 
      return redirect('/login');
    }

    public function username(){
       return 'login';
    }



}
