@extends('layouts.app') 
@section('title','Consulta de visitas del Boletín Judicial CDMX')
@section('content') 
@include('msj')

<h3 class="text-center">Consulta  de visitas del Boletín Judicial PJCDMX </h3> 
<br>
  <br>
  <form class="form-group" method="POST" action="{{ url('/consultavisitas/filtrar') }}" id="">          
       {{ csrf_field() }}

       <div class="form-group row justify-content-md-center">
          <div class="col-2">
              {!! Form::label('lblFechaInicial', 'Fecha Inicial :  ', array('for' => 'txtFechaInicial','class'=>'col-form-label')) !!}    
              {!! Form::date('fechainicial', $fechainicial); !!}
              
          </div>    
          <div class="col-2">
              {!! Form::label('lblFechaFinal', 'Fecha Final : ', array('for' => 'txtFechaFinal','class'=>'col-form-label')) !!}    
              {!! Form::date('fechafinal', $fechafinal); !!}
          </div> 
        </div>  
        <br>
        <div class="row justify-content-md-center">           
              <div class="col-1">
                 <button type="submit" class="btn btn-success"><span><i class="fa fa-filter" aria-hidden="true"></i></span> Filtrar</button>
              </div>               
        </div>
        <br><br><br>
 

        <div class="card">
        <div class="card-body">        
            <div class="form-group row justify-content-md-center">               
                <table class="table table-bordered" id="MyTable">
                    <thead>
                        <tr style="border: 1px solid"> 
                            <th class="text-center" >Número</th>  
                            <th class="text-center" >fecha</th> 
                            <th class="text-center" >Visitas</th>                            
                            <!--- <th class="text-center" >Descargas</th>   -->                                       
                        </tr>
                    </thead>  

                    <tbody>
                        @php $consecutivo=0 @endphp 
                        @php $totalvisitas=0 @endphp
			            @php $totaldescargas=0 @endphp 

                        @foreach($visitas as $visita)   
                        @php $consecutivo += 1  @endphp     
                        @php $totalvisitas += $visita->contador  @endphp
			            @php $totaldescargas += $visita->descargas  @endphp
                        <tr>                         
                                <td class="text-center" >{{$consecutivo}}</td>                        
                                <td class="text-center" >{{$visita->fecha_consulta}}</td>
                                <td class="text-center" >{{$visita->contador}}</td>
                               <!---  <td class="text-center" >{{$visita->descargas}} </td>  -->                                                                                             
                            </tr>                                                                                        
                        @endforeach
                    </tbody> 
                    <tfoot>
                        <tr style="border: 1px solid"> 
                            <th class="text-center" > </th>  
                            <th class="text-center" >Total</th> 
                            <th class="text-center" > {{$totalvisitas}} </th>                            
                            <!--- <th class="text-center" > {{$totaldescargas}} </th> -->                                         
                        </tr>
                    </tfoot>        
                </table>
            </div>   
        </div>
        </div>
</form>   
@endsection