<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Consulta Boletín Interno</title>
  <meta name="viewport" content="width=device-width,initial-scale=1.0">
  <link rel="stylesheet" href="{{ asset('PDFFlip/pflip/css/pdfflip.css') }}">

</head>
<body>

    <div class="PDFFlip" id="PDFF" source="{{asset('pdf/boletines/'.$boletin->titulo)}}">

   <script src="{{ asset('PDFFlip/pflip/js/libs/jquery.min.js') }}" type="text/javascript"></script>    
   <script src="{{ asset('PDFFlip/pflip/js/pdfflip.js') }}" type="text/javascript"></script>  
   <script src="{{ asset('PDFFlip/settingsin.js') }}" type="text/javascript"></script>
   <script src="{{ asset('PDFFlip/toc.js') }}" type="text/javascript"></script>
</body>
</html>
