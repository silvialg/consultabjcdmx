<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
//use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Route;
use App\Models\Otros\filtro;
use App\Models\boletin;
use App\Models\contador;
use App\Models\visitantes;
use App\Models\Post;
use App\Models\BitacoraAccion;
use Carbon\Carbon;

class ConsultaVisitantesController extends Controller
{
    public function index()
    {
        $fechaboletin = new \DateTime();
        $fechaboletin = $fechaboletin->format('Y-m-d');
        $fechafinal = new \DateTime();
        $fechafinal = $fechafinal->format('Y-m-d');
        $fechaini = Carbon::now();       
        $fechainicial = $fechaini->subDays(7, 'day');
        $fechainicial = $fechainicial->format('Y-m-d');

        $visitas = contador::where('fecha_consulta','>=', $fechainicial)
        ->where('fecha_consulta','<=', $fechafinal)
        ->orderby('fecha_consulta', 'DESC')
        ->get();      

        return view('reportes.consultavisitas',compact('visitas','fechafinal','fechainicial'));   
      
    }

    public function filtrar(Request $request)
    {               
        $fechaboletin = new \DateTime();
        $fechaboletin = $fechaboletin->format('Y-m-d');
        $fechafinal = $request->fechafinal;
        $fechainicial = $request->fechainicial;  
      
        $visitas = contador::where('fecha_consulta','>=', $fechainicial)
        ->where('fecha_consulta','<=', $fechafinal)
        ->orderby('fecha_consulta', 'DESC')
        ->get();    
        return view('reportes.consultavisitas',compact('visitas','fechafinal','fechainicial'));   
       
       
            
    }


}
