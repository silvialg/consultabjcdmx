<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBoletinTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('boletin', function (Blueprint $table) {
            $table->id();           
            $table->string('titulo');
            $table->string('nom_busqueda');
            $table->string('descripcion');
            $table->string('tipo');
            $table->string('ruta');          
            $table->date('fecha_boletin');
            $table->string('usuario');  
            $table->integer('iestatus')->default(1);    
            $table->timestamps();


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('boletin');
    }
}
