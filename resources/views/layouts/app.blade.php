<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>CONSULTA BOLETIN</title> 
    <!-- Scripts-->
     <script src="{{ asset('js/app.js') }}" defer></script>  

    <!-- Fonts 
    <link rel="dns-prefetch" href="//fonts.gstatic.com">-->

    <link rel="stylesheet" href="{{asset('bootstrap_4_4_1/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('css/bootstrap.css')}}">
    <link rel="stylesheet" href="{{asset('css/jquery.dataTables.min.css')}}">
    <link rel="stylesheet" href="{{asset('css/datatables.min.css')}}">
    <link rel="stylesheet" href="{{asset('css/buttons.dataTables.min.css')}}">
    <link rel="stylesheet" href="{{ asset('pflip/css/pdfflip.css') }}">
    
  
</head>
<body>
    <header>
       <img src="{{asset('/img/heder1920.png')}}" srcset="{{asset('/img/header1920.png 1920w')}}" sizes="(min-width: 1920px)" alt="Consulta">
    </header>
    <div id="app">
     
       <nav class="navbar navbar-expand-md navbar-light bg-white shadow-sm">       
            <div class="container">

            <div class="navbar-header">
            
                <!-- Collapsed Hamburger -->
                <button type="button" class="navbar-toggler" data-toggle="collapse" data-target="#app-navbar-collapse" aria-expanded="false" aria-label="Toggle navigation" aria-controls="navbarSupportedContent">
                <span class="sr-only">Toggle Navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                </button>
                </div> 
                
                <div class="collapse navbar-collapse" id="app-navbar-collapse">
                @auth
                    <!-- Left Side Of Navbar -->
                    <ul class="navbar-nav mr-auto">

                      @if(Auth::user()->roleID == 2)
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="color:#000000;"> Administrar </a>
                            <!-- FFV crea el Menú de opciones -->
                            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                   <a class="dropdown-item" href="{{ url('/subirboletin/') }}">
                                        <span><i class="fa fa-file" aria-hidden="true"></i></span> Administrar Boletín Judicial
                                    </a>                                                       
                                   
                            </div>

                        </li>
                     @endif()
                     
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="color:#000000;"> Consultar </a>
                            <!-- FFV crea el Menú de opciones -->
                            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                   <a class="dropdown-item" href="{{ url('/consultaboletinj/') }}">
                                        <span><i class="fa fa-eye" aria-hidden="true"></i></span> Consultar Boletín Judicial
                                    </a>
                                   
                            </div>
                        </li>
                        @if(Auth::user()->roleID == 2)
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="color:#000000;"> Reportes </a>
                            <!-- FFV crea el Menú de opciones -->
                            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                   <a class="dropdown-item" href="{{ url('/consultavisitas/') }}">
                                        <span><i class="fa fa-user-secret fa-lg" aria-hidden="true"></i></span> Consulta de Visitas
                                    </a>  
                                    <a class="dropdown-item" href="{{ url('/consultavisitasinternas/') }}">
                                        <span><i class="fa fa-file-text-o" aria-hidden="true"></i></span> Consulta de Visitas internas
                                    </a>                                   
                            </div>
                            
                        </li>
                        @endif()
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="color:#000000;"> Ayuda </a>
                            <!-- FFV crea el Menú de opciones -->
                            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                   <a class="dropdown-item" href="{{ url('/manual/') }}">
                                        <span><i class="fa fa-folder-open" aria-hidden="true"></i></span> Manual de usuario
                                    </a>
                                   
                            </div>
                        </li>
                    </ui> 
                    @endauth    
                </div>    
               
                <a class="navbar-brand" href="{{ url('/') }}">
                    <!--   {{ config('app.name', 'Laravel') }} -->
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->
                    <ul class="navbar-nav mr-auto">

                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ml-auto">
                        <!-- Authentication Links -->
                        @guest
                            @if (Route::has('login'))
                              <!-- 
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('login') }}">{{ __('Accesar') }}</a>
                                </li>
                              -->  
                            @endif

                            @if (Route::has('register'))
                               <!-- 
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('register') }}">{{ __('Registrarse') }}</a>
                                </li>
                                  -->  
                            @endif
                        @else
                            <li class="nav-item dropdown">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    {{ Auth::user()->name }}
                                </a>

                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Salir') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                        @csrf
                                    </form>
                                </div>
                            </li>
                        @endguest
                    </ul>
                    
                </div>
                
            </div>
        </nav>

        <main class="py-4">
            @yield('content')            
        </main>
    </div>
   
    <!--JS - JQuery -->
    <script type="text/javascript" src="{{ asset('js/jquery-3.3.1.js') }}"></script>    
    <script type="text/javascript" src="{{asset('js/jquery-3.3.1.js')}}" defer></script>
    <script type="text/javascript" src="{{asset('js/jquery-3.3.1.min.js')}}"></script>   
    <script type="text/javascript" src="{{asset('js/datatables.min.js')}}" defer></script>
    <script type="text/javascript" src="{{asset('js/dataTables.buttons.min.js')}}" defer></script>   
    <script src="{{ asset('PDFFlip/pflip/js/libs/jquery.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('PDFFlip/pflip/js/pdfflip.js') }}" type="text/javascript"></script>
    <script src="{{ asset('PDFFlip/settings.js') }}" type="text/javascript"></script>
    <script src="{{ asset('PDFFlip/toc.js') }}" type="text/javascript"></script>
    <script src="{{ asset('js/mistablas.js') }}" type="text/javascript"></script>

  
</body>
<footer>
    <img src="{{asset('/img/footer1920.jpg')}}" srcset="{{asset('/img/footer1920.jpg 1920w')}}" sizes="(min-width: 1920px)" alt="Ejemplo">
</footer>
</html>