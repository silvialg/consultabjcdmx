<!-- Ventana modal para eliminar -->
<div class="modal fade" id="deleteBoletin{{ $boletin->id }}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header" >
                <h4 class="modal-title text-center">
                <span>¿Realmente deseas eliminar este boletín ? </span>
                </h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button> 
            </div>
            <div class="modal-body mt-5 text-center">
                <strong style="text-align: center !important"> {{$boletin->titulo }} de fecha {{ $boletin->fecha_boletin  }} </strong>
            </div>
            <div class="modal-footer"><button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                <a  class="btn btn-danger" href="{{ route('admin.deleteBoletin', $boletin->id)}}">Borrar</a>
            </div>
        </div>
    </div>
</div>
<!---fin ventana eliminar---> 
