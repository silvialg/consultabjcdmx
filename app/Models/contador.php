<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class contador extends Model
{
    use HasFactory;
    protected $table="contadores";
    protected $primarykey = ["id"];
    public $timestamps = false;
    protected $fillable=['id', 'fecha_consulta','contador','ip','pagina','url'];
 
}