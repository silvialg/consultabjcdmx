@extends('layouts.app') 

@section('title','Subir Archivos de Boletín Judicial')
@section('content') 
@include('msj')


<h3 class="text-center">Administración Boletín Judicial PJCDMX</h3> 
  <br>
  <br>
  <form class="form-group" method="POST" action="{{ url('/admboletin/filtrar') }}" id="">    
       {{ csrf_field() }}

       <div class="form-group row justify-content-md-center">
          <div class="col-3">
              {!! Form::label('lblFechaInicial', 'Fecha Inicial   ', array('for' => 'txtFechaInicial','class'=>'col-form-label')) !!}    
              {!! Form::date('fechainicial', $fechainicial); !!}
              
          </div>    
          <div class="col-3">
              {!! Form::label('lblFechaFinal', 'Fecha Final  ', array('for' => 'txtFechaFinal','class'=>'col-form-label')) !!}    
              {!! Form::date('fechafinal', $fechafinal); !!}
          </div> 
        </div>  
        <br>
        <br>
        <div class="row justify-content-md-center">           
              <div class="col-3">
                 <button type="submit" class="btn btn-success"><span><i class="fa fa-filter" aria-hidden="true"></i></span> Filtrar</button>
              </div>
                 <a href="#newBoletinModal" data-idgrupo="" class="btn btn-info" data-toggle="modal" data-target="#newBoletinModal"><i class="fa fa-file" aria-hidden="true"></i> Nuevo Boletín Judicial</a>     
       
        </div>
        <br><br><br>

        <div class="card">
          <div class="card-body">        
              <div class="form-group row justify-content-md-center">               
                   <table class="table table-bordered" id="MyTable">
                      <thead>
	    		               <tr style="border: 1px solid"> 
					                  <th class="text-center" >No.</th>  
						               
                            <th class="text-center" >Tipo</th>                            
                            <th class="text-center" >Fecha</th>   
                            <!-- <th class="text-center" >Ruta</th>
                            <th class="text-center" >Nombre</th>   -->                   
                            <th class="text-center" >Acciones</th>                                      
		                     </tr>
		                   </thead>  

                       <tbody>
                         @php $consecutivo=0 @endphp 
                          @foreach($boletines as $boletin) 
                              @php $consecutivo += 1  @endphp        
                              <tr>                         
                                 <!-- <td class="text-center" >{{$boletin->id}}</td>	-->
                                <td class="text-center" >{{$consecutivo}}</td>  										
                                @if ($boletin->tipo == 1)
                                     <td class="text-center" >Público</td>
                                @else 
                                    <td class="text-center" >Interno</td>
                                 @endif 
                               
                                <td class="text-center" >{{ \Carbon\Carbon::parse($boletin->fecha_boletin)->translatedFormat('d-M-Y') }} </td>                               
                                 <!--<td class="text-center" >{{$boletin->ruta}}</td>
                                 <td class="text-center" >{{$boletin->titulo}}</td>    -->                     
                                <td class="text-center" >   
                                  
                                &nbsp;&nbsp;&nbsp;        
                                                       
                                        <!-- <a href="{{ asset('pdf/boletines/'.$boletin->ruta) }}" class="btn btn-info" > <span class="fa fa-eye" aria-hidden="true">Ver</a> 
                                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#verBoletin{{$boletin->id}}"><i class="fa fa-eye" aria-hidden="true"></i></button> -->                                        
                                        <a title="Visualizar el archivo del boletín" class="btn btn-primary" href="{{ url('externo', $boletin->id) }}"><i class="fa fa-eye" aria-hidden="true"></i></a>                                         
                                        &nbsp;
                                        <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#deleteBoletin{{ $boletin->id }}"><i class="fa fa-trash" aria-hidden="true"></i></button>                                
                                        &nbsp;
                                         <!--  <a title="Visualizar el archivo del boletín" class="btn btn-primary" href="{{ url('externocosedora', $boletin->id) }}"><i class="fa fa-eye" aria-hidden="true"></i></a> -->                                            
                                        
                                </td>									
                              </tr>  
                                    
                                <!--Ventana Modal para Actualizar--->                        
                                 @include('admin.deleteBoletin')  
                                                           
	                        @endforeach
	                     </tbody> 
                       <tfoot>
	              	         <tr style="border: 1px solid"> 
                              <th class="text-center" >No.</th>  
						                 
                              <th class="text-center" >Tipo</th>                    
                              <th class="text-center" >Fecha</th>   
                              <!-- <th class="text-center" >Ruta</th>
                              <th class="text-center" >Nombre</th>   -->                    
                              <th class="text-center" >Acciones</th>      
	                          </tr>
	                      </tfoot>        
                </table>
            </div>   
       </div>
  </div>
</form>




  
<!-- Subir Archivo PDF-->
<div class="modal fade" id="newBoletinModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" enctype =  "multipart/form-data">
    <div class="modal-dialog modal-lg" role="document">
          <div class="modal-content">
               <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Subir Boletín Judicial PJCDMX</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                      <form class="form-group" method="POST" action="{{ url('/admboletin/subirboletin') }}" id="formboletin" enctype="multipart/form-data">
                        {{ csrf_field() }}                
                        <div class="form-group row justify-content-md-center">
                          <div class="col-4">
                            <label for="tipob" class="col">Tipo de Boletín</label>
                          </div>
                          <div class="col-6">
                               <select name="tipob" class="form-control" id="tipob" required>
                                     <option value="" >Seleccionar el tipo de boletín</option>                                            
                                        <option value= "1">Público</option> 
                                        <option value= "2">Interno</option>                                  
                                </select>
                          </div>
                        </div>
                     
                        <div class="form-group row justify-content-md-center">
                          <div class="col-4">
                            <label for="tipob" class="col">Fecha del Boletín</label>
                          </div>
                          <div class="col-6">                         
                               {!! Form::date('fechaboletin', $fechaboletin); !!}
                          </div>                                                  
                           
                        </div>                    
                        <div class="form-group row justify-content-md-center">
                          <div class="col-4">
                            <label for="archivo" class="col">Archivo PDF</label>
                          </div>
                          <div class="col-6">
                            <input type="file" name="urlpdf" id="urlpdf" class="form-control" value="" accept="pdf">
                                <label for="file-1">
                                <svg xmlns="http://www.w3.org/2000/svg" class="iborrainputfile" width="20" height="17" viewBox="0 0 20 17"><path d="M10 0l-5.2 4.9h3.3v5.1h3.8v-5.1h3.3l-5.2-4.9zm9.3 11.5l-3.2-2.1h-2l3.4 2.6h-3.5c-.1 0-.2.1-.2.1l-.8 2.3h-6l-.8-2.2c-.1-.1-.1-.2-.2-.2h-3.6l3.4-2.6h-2l-3.2 2.1c-.4.3-.7 1-.6 1.5l.6 3.1c.1.5.7.9 1.2.9h16.3c.6 0 1.1-.4 1.3-.9l.6-3.1c.1-.5-.2-1.2-.7-1.5z"></path></svg>
                                <span class="iborrainputfile">Seleccionar archivo</span>
                                </label>

                          </div>
                        </div>
                       
                          <div class="modal-footer">
                          <div id="">
                            <button type="submit" class="btn btn-info" id="btnAddEspecialidad"><span><i class="fa fa-book" aria-hidden="true"></i></span> Cargar Boletín</button>
                          </div>
                          </div>
                      </form>
                  </div>
            </div>
    </div>
  </div>


@endsection
