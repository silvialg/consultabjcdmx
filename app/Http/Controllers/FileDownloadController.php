<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;


class FileDownloadController extends Controller
{
    public function index()
    {
       echo $filePath = public_path("pdf\boletines\prueba.pdf");
       $headers = ['Content-Type: application/pdf'];
       $fileName = time().'.pdf';
       return response()->download($filePath, $fileName, $headers);
    }
}
