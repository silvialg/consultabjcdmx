<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\FileDownloadController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    return view('welcome');
//});

//Auth::routes();

Auth::routes();


Route::group(['middleware' => 'web'], function(){
    Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
});

Route::group(['middleware'=>'auth'],function(){
    Route::get('/subirboletin', [App\Http\Controllers\AdmBoletinController::class, 'index'])->name('boletin.index');
    Route::post('/admboletin/filtrar', [App\Http\Controllers\AdmBoletinController::class, 'filtrar'])->name('boletin.filtrar');
    Route::post('/admboletin/subirboletin',[App\Http\Controllers\AdmBoletinController::class, 'subirboletin']);
    Route::get('admin/deleteBoletin/{id}',[App\Http\Controllers\AdmBoletinController::class, 'deleteBoletin'])->name('admin.deleteBoletin');

    Route::get('/consultaboletinj', [App\Http\Controllers\ConsultaBoletinController::class, 'index'])->name('consultaboletin.index');
    Route::post('/consultaboletinj/filtrar', [App\Http\Controllers\ConsultaBoletinController::class, 'filtrar'])->name('consultaboletin.filtrar');
    Route::get('/consultaboletinj/descargarboletin/{id}', [App\Http\Controllers\ConsultaBoletinController::class, 'descargarboletin'])->name('consultaboletin.descargarboletin');
    Route::get('/consultaboletinj/descargar/{id}', [App\Http\Controllers\ConsultaBoletinController::class, 'descargar'])->name('descargar');
    Route::get('/consultaboletinj/downfile/{id}', [App\Http\Controllers\ConsultaBoletinController::class, 'downfile'])->name('downfile');
    
    Route::get('/manual', [App\Http\Controllers\ManualUsuarioController::class,'index']);//Descargar el manual de usuario
    
      
    Route::get('/consultavisitas', [App\Http\Controllers\ConsultaVisitantesController::class, 'index'])->name('consultavisitas.index');
    Route::post('/consultavisitas/filtrar', [App\Http\Controllers\ConsultaVisitantesController::class, 'filtrar'])->name('consultavisitas.filtrar');

    Route::get('/consultavisitasinternas', [App\Http\Controllers\VisitasInternasController::class, 'index'])->name('index');
    Route::post('/visitasinternas/filtrar', [App\Http\Controllers\VisitasInternasController::class, 'filtrar'])->name('visitasinternas.filtrar');

});

Route::get('/consultaboletinpjcdmx', [App\Http\Controllers\ConsultaBoletinpjcdmxController::class, 'index'])->name('consultaboletinpjcdmx.index');
Route::post('/consultaboletinpjcdmx/filtrar', [App\Http\Controllers\ConsultaBoletinpjcdmxController::class, 'filtrar'])->name('consultaboletinpjcdmx.filtrar');
Route::get('/consultaboletinpjcdmx/descargarboletin/{id}', [App\Http\Controllers\ConsultaBoletinpjcdmxController::class, 'descargarboletin'])->name('consultaboletinpjcdmx.descargarboletin');
Route::get('/consultaboletinpjcdmx/verboletin/{id}', [App\Http\Controllers\ConsultaBoletinpjcdmxController::class, 'verboletin'])->name('consultaboletinpjcdmx.verboletin');
Route::get('info', [App\Http\Controllers\InfoController::class, 'index'])->name('info.index'); 
Route::get('externo/{id}', [App\Http\Controllers\ConsultaBoletinpjcdmxController::class, 'visualizar'])->name('visualizar');
Route::get('verexterno', function(){ return view('verexterno');
    
});