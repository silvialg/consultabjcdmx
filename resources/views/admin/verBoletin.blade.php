<!-- Ventana modal para eliminar -->
<div class="modal fade" id="verBoletin{{ $boletin->id }}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-xl modal-dialog-centered" role="document">  
      <div class="modal-content">
        <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Consulta del Boletin Judicial de fecha  {{ \Carbon\Carbon::parse($boletin->fecha_boletin)->translatedFormat('d-M-Y') }}  </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            <div class="modal-body mt-5 text-center">
                  <!--- <iframe width="850" height="600" src="{{asset('pdf/boletines/'.$boletin->ruta.'#toolbar=0')}}" frameborder="0"></iframe>  --->
                  <embed  src = "{{asset('pdf/boletines/'.$boletin->ruta.'#toolbar=0')}}"  type = "application/pdf"  width = "850px"  height = "600px" />
                   
            </div>
            <div class="modal-footer"><button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
              
            </div>
        </div>
    </div>
</div>
<!---fin ventana eliminar---> 
