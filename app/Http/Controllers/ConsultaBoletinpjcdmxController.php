<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
//use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Http;
use App\Models\Otros\filtro;
use App\Models\boletin;
use App\Models\contador;
use App\Models\visitantes;
use App\Models\Post;
use App\Models\BitacoraAccion;
use Carbon\Carbon;
use SoapClient;
use GuzzleHttp\Client;

class ConsultaBoletinpjcdmxController extends Controller
{
    public function index()
    {
                
         $fecha_dia = date("Y-m-d");
         $hora_dia = date("H:i:s");
         $ip = $_SERVER["REMOTE_ADDR"] ?? ""; 
        
        $contadorv = contador::where('fecha_consulta', '=', $fecha_dia)                   
        ->count();      

        if ($contadorv == null){           
            $visitas = new contador();
            $visitas->fecha_consulta =  $fecha_dia;    
            $visitas->contador =  1;                       
            $visitas->created_at = Carbon::now();
            $visitas->save();             
        }
        else { 
            DB::table('contadores')
            ->where('fecha_consulta', '=', $fecha_dia) 
           ->update(array('contador' =>DB::raw('contador + 1')));                   
        }
       
        //codigo visitantes a la consulta  
        // $nombreRuta = Request::route()->getName();      
        //$visitas = new visitantes();
        //$visitas->fecha_consulta =  $fecha_dia;
        //$visitas->hora_consulta =   $hora_dia;     
        //$visitas->ip = $ip;  
        //$visitas->pagina = 1;  
        //$visitas->pagina = $nombreRuta;                       
        //$visitas->created_at = Carbon::now();
        //$visitas->save();        

        
        $contador =  1;         
        $fechaboletin = new \DateTime();
        $fechaboletin = $fechaboletin->format('Y-m-d');
        $fechafinal = new \DateTime();
        $fechafinal = $fechafinal->format('Y-m-d');
        $fechaini = Carbon::now();       
        $fechainicial = $fechaini->subDays(7, 'day');
        $fechainicial = $fechainicial->format('Y-m-d');
       

        $boletines = boletin::where('iestatus', '=', 1)
         ->where('tipo','=', 1)
         ->where('fecha_boletin','>=', $fechainicial)
         ->where('fecha_boletin','<=', $fechafinal)
         ->orderby('fecha_boletin', 'DESC')
         ->get();      
         
         

        return view('consulta.consultaboletinpjcdmx',compact('boletines','fechafinal','fechainicial','fechaboletin','contador'));      
              
    }

    public function filtrar(Request $request)
    {
        $fecha_dia = date("Y-m-d");
        $hora_dia = date("H:i:s");
        //codigo para las visitas a la consulta del boletin 
        $fecha_dia = date("Y-m-d");
        $ip = $_SERVER["REMOTE_ADDR"] ?? ""; 
        
        //codigo para las visitas a la consulta del boletin        
        DB::table('contadores')
        ->where('fecha_consulta', '=', $fecha_dia) 
        ->update(array('contador' =>DB::raw('contador + 1')));                   
        
        $fechaboletin = new \DateTime();
        $fechaboletin = $fechaboletin->format('Y-m-d');
        $fechafinal = $request->fechafinal;
        $fechainicial = $request->fechainicial;  
        $boletines = boletin::where('iestatus', '=', 1)
        ->where('tipo','=', 1)
        ->where('fecha_boletin','>=', $fechainicial)
        ->where('fecha_boletin','<=', $fechafinal)
        ->orderby('fecha_boletin', 'DESC')
        ->get();      
        
        //$visitas = new visitantes();
        //$visitas->fecha_consulta =  $fecha_dia;
        //$visitas->hora_consulta =   $hora_dia;     
        //$visitas->ip = $ip;  
        //$visitas->pagina = 1;                                 
        //$visitas->created_at = Carbon::now();
        //$visitas->save(); 
        
        return view('consulta.consultaboletinpjcdmx',compact('boletines','fechafinal','fechainicial','fechaboletin'));    
            
    }


    public function descargarboletin($id, Request $request)
    {
        echo $fecha_dia = date("Y-m-d");
        //codigo para las visitas a la descargas del boletin        
        DB::table('contadores')
        ->where('fecha_consulta', '=', $fecha_dia) 
        ->update(array('descargas' =>DB::raw('descargas + 1')));                   
        
        $Datos = boletin::find($id);      
        $pathToFile = "pdf/boletines/".$Datos->ruta;
        return response()->download($pathToFile);
            
    }



    public function verboletin($id, Request $request)
    {
                
                
        
        $Datos = boletin::find($id);      
        return view('consulta.verexternocompleto',compact('Datos'));     
       
              
    }


    public function visualizar(Request $request,$id)
    {
         // echo $id;
         //$fecha_publica = "BOLETIN JUDICIAL DEL DIA ".date("d/m/Y");
         $boletin = boletin::find($id);   
         $datos = $boletin->descripcion;  
         $fecha_publica = "BOLETIN JUDICIAL DEL DIA ".\Carbon\Carbon::parse($boletin->fecha_boletin)->translatedFormat('d/m/Y');
          // $documentos_arr_final[0]['id_gestor']="13146141";
        $documentos_arr_final[0]['id_gestor']=$datos;
        $documentos_arr_final[0]['indice_text']=$fecha_publica;
        $documentos_arr_final[0]['visible']=1;

        unset($datos_final);
           $datos_final["sistema"]="SIGJ";
           //configuracion del visor
           $datos_final["visor"]["sonido"]="0";
           $datos_final["visor"]["autoplay"]="1";
           $datos_final["visor"]["imprimir"]="0";
           $datos_final["visor"]["descargar_pdf"]="0";
           $datos_final["visor"]["descargar_pagina"]="0";
           $datos_final["visor"]["fullscreen"]="0";
           $datos_final["visor"]["marca_agua"]="0";
           $datos_final["visor"]["background_color"]="0";
           $datos_final["visor"]["background_img"]="0";
           //datos del expediente
           $datos_final["expediente"]["numero"]=100;
           $datos_final["expediente"]["anio"]=2021;
           $datos_final["expediente"]["juzgado"]="100PIC";
           $datos_final["expediente"]["tipo"]="Expediente";
           $datos_final["expediente"]["bis"]="";
           $datos_final["expediente"]["cuaderno"]="";
           $datos_final["expediente"]["alias"]="";
           //datos del usuario
           //$datos_final["usuario"]["id"]=$request->session()->get('usuario_id');
           //$datos_final["usuario"]["nombre"]=$request->session()->get('usuario_nombre_completo');
           //$datos_final["usuario"]["tipo"]=$request->session()->get('tipo_usuario_descripcion');
           //pdf b64
           $datos_final['cubierta_b64'] = "";
           $datos_final['contraportada_b64'] = "";
           $datos_final['solapa_b64'] = "";
           //documentos
           $datos_final['contenido_arr']=$documentos_arr_final;


           $cliente_cosedora   =  new  Client ([
               'base_uri'  =>  'http://edigital.poderjudicialcdmx.gob.mx/public/' ,      
           ]);
           $request->clienteWS_cosedora = $cliente_cosedora;
           $response = $request
              ->clienteWS_cosedora
              ->request('POST', 'consulta_expediente',[
                "json" =>
                $datos_final
            ,
            "headers" => [
                "Content-Type" => "application/json",
                "auth" => "LJJWvG9cdiSLRQ"
            ]
           ]);
           $response = json_decode($response->getBody(),true) ;
       return view('externo',compact('boletin','response'));    
            
    }

   
}
