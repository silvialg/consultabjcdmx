<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBitacoraTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bitacora', function (Blueprint $table) {
            $table->increments('id'); //integer 
            $table->integer('id_usuario')->nullable();
            $table->date('fecha');
            $table->time('hora');            
            $table->string('accion');
            $table->string('id_general'); 
            $table->string('id_entidad');
            $table->string('ip');          
            $table->timestamps(); //created at and updated at            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bitacora');
    }
}
