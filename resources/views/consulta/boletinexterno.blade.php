<!-- Ventana modal para eliminar -->
<div class="modal fade" id="boletinexterno{{ $boletin->id }}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-xl modal-dialog-centered" role="document">  
      <div class="modal-content">
        <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Consulta del Boletin Judicial de fecha  {{ \Carbon\Carbon::parse($boletin->fecha_boletin)->translatedFormat('d-M-Y') }}   </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            <div class="modal-body mt-5 text-center">
                  <embed  src = "{{asset('pdf/boletines/'.$boletin->ruta.'#toolbar=0')}}"  type = "application/pdf"  width = "850px"  height = "600px" contextmenu ="false"  />  
                 <!---  
                   <iframe id="fraDisabled" width="850" height="600" src="{{asset('pdf/boletines/'.$boletin->ruta.'#toolbar=0')}}" onload="disableContextMenu();" onMyLoad="disableContextMenu();"></iframe> 
                   <embed  src = "{{asset('pdf/boletines/'.$boletin->ruta.'#toolbar=1')}}"  type = "application/pdf"  width = "850px"  height = "600px" contextmenu ="false"  /> 
                   <embed  src = "{{asset('pdf/boletines/'.$boletin->ruta.'#toolbar=0')}}"  type = "application/pdf"  width = "850px"  height = "600px" contextmenu ="false"  />                  
                   <iframe src="http://docs.google.com/gview?url=http://www.pdf995.com/samples/pdf.pdf&embedded=true" style="width:500px; height:500px;" frameborder="0"></iframe>--> 
 
                  <!---  <iframe src="{{asset('pdf/boletines/'.$boletin->ruta.'#toolbar=0')}}" style="width:850px; height:600px;" frameborder="0"></iframe>  --> 
                  
                  @php                 
                  $fullurl = url('pdf/boletines/'.$boletin->ruta);                    
                  $fullurle = 'https://www.poderjudicialcdmx.gob.mx/wp-content/PHPs/boletin/boletin_repositorio/010320221.pdf';    
                  @endphp               
                 <!---    <iframe 
                        src="http://docs.google.com/gview?url={{ $fullurl }}&embedded=true" 
                        style="width:600px; height:500px;" 
                        frameborder="0">
                         </iframe>   -->                                                                                  
            </div>
            <div class="modal-footer"><button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
              
            </div>
        </div>
    </div>
</div>
