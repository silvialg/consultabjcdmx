<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVisitantesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('visitantes', function (Blueprint $table) {
            $table->id();
            $table->date('fecha_consulta');
            $table->time('hora_consulta');       
            $table->string('ip') ->nullable();;
            $table->string('pagina') ->nullable();;
            $table->string('url') ->nullable();;
            $table->string('cordenada') ->nullable();;
            $table->string('id_empleado') ->nullable();;
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('visitantes');
    }
}
