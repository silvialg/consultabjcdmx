<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class visitantes extends Model
{
    use HasFactory;
    protected $table="visitantes";
    protected $primarykey = ["id"];
    public $timestamps = false;
    protected $fillable=['id', 'fecha_consulta', 'hora_consulta','ip','pagina','url','cordenada'];
 
}