
@extends('layouts.app') 

@section('content') 
@include('msj')
<h3 class="text-center">Consulta del Boletín Judicial PJCDMX </h3> 
<br>
  <br>
  <form class="form-group" method="POST" action="{{ url('/consultaboletinpjcdmx/filtrar') }}" id="">    
       {{ csrf_field() }}

       <div class="form-group row justify-content-md-center">
          <div class="col-2">
              {!! Form::label('lblFechaInicial', 'Fecha Inicial :  ', array('for' => 'txtFechaInicial','class'=>'col-form-label')) !!}    
              {!! Form::date('fechainicial', $fechainicial); !!}
              
          </div>    
          <div class="col-2">
              {!! Form::label('lblFechaFinal', 'Fecha Final : ', array('for' => 'txtFechaFinal','class'=>'col-form-label')) !!}    
              {!! Form::date('fechafinal', $fechafinal); !!}
          </div> 
        </div>  
        <br>
        <div class="row justify-content-md-center">           
              <div class="col-1">
                 <button type="submit" class="btn btn-success"><span><i class="fa fa-filter" aria-hidden="true"></i></span> Filtrar</button>
              </div>               
        </div>
        <br><br><br>

        <div class="card">
          <div class="card-body">        
              <div class="form-group row justify-content-md-center">               
                   <table class="table table-bordered" id="MyTable">
                      <thead>
                         <tr style="border: 1px solid"> 
                            <th class="text-center" >No.</th>                                          
                            <th class="text-center" >Fecha</th>                                          
                            <th class="text-center" >Acciones</th>                                      
                         </tr>
                       </thead>  

                       <tbody>
                          @php $consecutivo=0 @endphp                     
                          @foreach($boletines as $boletin)                         			                     
                              @php $consecutivo += 1  @endphp        
                          
                              <tr>                         
                                <td class="text-center" >{{$consecutivo}}</td>                                                                                                                 
                                <td class="text-center" >{{ \Carbon\Carbon::parse($boletin->fecha_boletin)->translatedFormat('d-M-Y') }} 
                              
                              </td>
                                                    
                                <td class="text-center" >   
                                  
                                &nbsp;&nbsp;&nbsp;        
                                                                                             
                                          <a title="Visualizar el archivo del boletín" class="btn btn-primary" href="{{ url('externo', $boletin->id) }}"><i class="fa fa-eye" aria-hidden="true"></i></a>                                         
                                         

                                          <!-- 
                                            <button title="Visualizar el archivo del boletín" type="button" class="btn btn-primary" data-toggle="modal" data-target="#boletinexterno{{$boletin->id}}"><i class="fa fa-eye" aria-hidden="true"></i></button>                                       
                                            <a title="Visualizar el archivo del boletín" class="btn btn-primary" href="{{ url('/consultaboletinpjcdmx/verboletin', $boletin->id) }}"><i class="fa fa-eye" aria-hidden="true"></i></a>               
                                            <a title="Descargar el archivo del boletín" class="btn btn-success" href="{{ url('verexterno') }}"><i class="fa fa-folder-open" aria-hidden="true"></i></a>   
                                           <a href="{{asset('pdf/boletines/'.$boletin->titulo)}}" download="A-38.pdf" class="btn btn-info"> <span class="fa fa-file-pdf-o" aria-hidden="true"></a>      
                                            
                                          
                                         -->      
                                     
                                </td>                 
                              </tr>  
                                    
                               
                                   <!--Ventana Modal para Actualizar--->                        
                                 @include('consulta.boletinexterno')                                   
                          @endforeach
                       </tbody> 
                       <tfoot>
                           <tr style="border: 1px solid"> 
                              <th class="text-center" >No</th>                                                                        
                              <th class="text-center" >Fecha</th>                   
                              <th class="text-center" >Acciones</th>      
                            </tr>
                        </tfoot>        
                </table>
            </div>   
       </div>
  </div>
</form>

 <!--<script src="{{ asset('js/contadorvisitas.js') }}"></script> -->   
  
@endsection
