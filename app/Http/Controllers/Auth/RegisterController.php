<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use App\Models\User;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use GuzzleHttp\Client;//usar guzzle para consumir WS
use App\Traits\VerificaempTrait;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Auth;
use Illuminate\Auth\Events\Registered;
use SoapClient;


class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;
    use VerificaempTrait; //AGREGAMOS EL TRAIT

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::REGISTER;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }
    public function showRegistrationForm()
    {
        return view('auth.register');
    }
    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
            'rfc' => ['required', 'string', 'max:13'],
            'roleID' => [ 'string', 'max:1'],
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\Models\User
     */
    protected function create(array $data)
    {
       $nombre = $data['name'];
       $idp    = $data['nempleado'];  
       $rfc    = $data['rfc'];
       $email  = $data['email'];
       $password =Hash::make($data['password']);
      
        // Get data using Trait method
        $valor = $this->getData($idp,$rfc);
        
        //dd($valor);
         if ($valor == '0')
         {
             return redirect()->route('register')
            ->with('msjdelete', 'EL EMPLEADO NO EXISTE, FAVOR DE VERIFICAR SUS DATOS');
         }else{
            Session::flash('usuario_creado','USUARIO CREADO DE CLIC EN ACCESAR PARA INGRESAR AL SISTEMA');
             /*return User::create([
                'name' => $data['name'],
                'nempleado' => $data['nempleado'],
                'email' => $data['email'],
                'password' => Hash::make($data['password']),
                'rfc' => $data['rfc'],
                'roleID' => '1',
             ]);*/
             $Datos = new user();
             $Datos->name       = $nombre ;     
             $Datos->nempleado  = $idp;             
             $Datos->email      = $email;       
             $Datos->password   = $password;       
             $Datos->rfc        = strtoupper($rfc);        
             $Datos->roleID     = '1';   
             $Datos->entidad    = $valor;       
             $Datos->save();
             $this->guard()->login($Datos);
         }
         
    }

    public function register(Request $request)
    {
        $this->validator($request->all())->validate();

        event(new Registered($user = $this->create($request->all())));
        return redirect()->route('home');
        /*$this->guard()->login($user);
 
        if ($response = $this->registered($request, $user)) {
            return $response;
        }

        return $request->wantsJson()
                    ? new JsonResponse([], 201)
                    : redirect($this->redirectPath());*/
    }

    protected function guard()
    {
        return Auth::guard();
    }

    /**
     * The user has been registered.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  mixed  $user
     * @return mixed
     */
    protected function registered(Request $request, $user)
    {
        //
    }

}
