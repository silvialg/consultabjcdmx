<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Route;
use App\Models\Otros\filtro;
use App\Models\boletin;
use App\Models\contador;
use App\Models\User;
use App\Models\visitantes;
use App\Models\Post;
use App\Models\BitacoraAccion;
use Carbon\Carbon;

class VisitasInternasController extends Controller
{
    public function index()
    {                 
        $ip = $_SERVER["REMOTE_ADDR"] ?? ""; 
        $fechaboletin = new \DateTime();
        $fechaboletin = $fechaboletin->format('Y-m-d');
        $fechafinal = new \DateTime();
        $fechafinal = $fechafinal->format('Y-m-d');
        $fechaini = Carbon::now();       
        $fechainicial = $fechaini->subDays(7, 'day');
        $fechainicial = $fechainicial->format('Y-m-d');
       
        $visi_int=DB::table('visitantes')
            ->select('users.name','id_empleado','fecha_consulta','ip',DB::raw('count(*) as con_visitas'))
            ->join('users','users.id','=','visitantes.id_empleado')
            ->where('fecha_consulta','>=', $fechainicial)
            ->where('fecha_consulta','<=', $fechafinal)
            ->groupBy('users.name')
            ->groupBy('id_empleado')
            ->groupBy('fecha_consulta')
            ->groupBy('ip')
            ->orderby('fecha_consulta', 'DESC')          
            ->get();


       return view('reportes.consultavisitasinternas',compact('visi_int','fechafinal','fechainicial'));    
         
    }

    public function filtrar(Request $request)
    {
                
        $ip = $_SERVER["REMOTE_ADDR"] ?? ""; 
        $fechaboletin = new \DateTime();
        $fechaboletin = $fechaboletin->format('Y-m-d');
        $fechafinal = new \DateTime();
        $fechafinal = $fechafinal->format('Y-m-d');
        $fechaini = Carbon::now();       
        $fechafinal = $request->fechafinal;
        $fechainicial = $request->fechainicial;  
       
        $visi_int=DB::table('visitantes')
            ->select('users.name','id_empleado','fecha_consulta','ip',DB::raw('count(*) as con_visitas'))
            ->join('users','users.id','=','visitantes.id_empleado')
            ->where('fecha_consulta','>=', $fechainicial)
            ->where('fecha_consulta','<=', $fechafinal)
            ->groupBy('users.name')
            ->groupBy('id_empleado')
            ->groupBy('fecha_consulta')
            ->groupBy('ip')
            ->orderby('fecha_consulta', 'DESC')         
            ->get();


       return view('reportes.consultavisitasinternas',compact('visi_int','fechafinal','fechainicial'));    
         
    }

    public function getUserIpAddr(){
    $ipaddress = '';
    if (isset($_SERVER['HTTP_CLIENT_IP']))
        $ipaddress = $_SERVER['HTTP_CLIENT_IP'];
    else if(isset($_SERVER['HTTP_X_FORWARDED_FOR']))
        $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
    else if(isset($_SERVER['HTTP_X_FORWARDED']))
        $ipaddress = $_SERVER['HTTP_X_FORWARDED'];
    else if(isset($_SERVER['HTTP_FORWARDED_FOR']))
        $ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
    else if(isset($_SERVER['HTTP_FORWARDED']))
        $ipaddress = $_SERVER['HTTP_FORWARDED'];
    else if(isset($_SERVER['REMOTE_ADDR']))
        $ipaddress = $_SERVER['REMOTE_ADDR'];
    else
        $ipaddress = 'UNKNOWN';    
    return $ipaddress;
 }



}
