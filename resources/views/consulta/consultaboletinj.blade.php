@extends('layouts.app') 
@section('title','Subir Archivos de Boletín Judicial')
@section('content') 
@include('msj')
<h3 class="text-center">Consulta del Boletín Judicial PJCDMX </h3> 
<br>
  <br>
  <form class="form-group" method="POST" action="{{ url('/consultaboletinj/filtrar') }}" id="">    
       {{ csrf_field() }}

       <div class="form-group row justify-content-md-center">
          <div class="col-2">
              {!! Form::label('lblFechaInicial', 'Fecha Inicial :  ', array('for' => 'txtFechaInicial','class'=>'col-form-label')) !!}    
              {!! Form::date('fechainicial', $fechainicial); !!}
              
          </div>    
          <div class="col-2">
              {!! Form::label('lblFechaFinal', 'Fecha Final : ', array('for' => 'txtFechaFinal','class'=>'col-form-label')) !!}    
              {!! Form::date('fechafinal', $fechafinal); !!}
          </div> 
        </div>  
        <br>
        <div class="row justify-content-md-center">           
              <div class="col-1">
                 <button type="submit" class="btn btn-success"><span><i class="fa fa-filter" aria-hidden="true"></i></span> Filtrar</button>
              </div> 
              
            

        </div>
        <br><br><br>

        <div class="card">
          <div class="card-body">        
              <div class="form-group row justify-content-md-center">               
                   <table class="table table-bordered" id="MyTable">
                      <thead>
	    		               <tr style="border: 1px solid"> 
					                  <th class="text-center" >No.</th>                     
                            <th class="text-center" >Fecha</th>   
                            <!-- <th class="text-center" >Ruta</th>
                             <th class="text-center" >Tipo</th>  -->                     
                            <th class="text-center" >Acciones</th>                                      
		                     </tr>
		                   </thead>  

                       <tbody>
                        @php $consecutivo=0 @endphp 
			                  @php $totaldescargas=0 @endphp 
                          @foreach($boletines as $boletin) 
                          @php $consecutivo += 1  @endphp        
                              <tr>                         
                                <td class="text-center" >{{$consecutivo}}</td>												                          
                                <td class="text-center" >{{ \Carbon\Carbon::parse($boletin->fecha_boletin)->translatedFormat('d-M-Y') }}  </td>                                                     
                                <td class="text-center" >   
                                  
                                &nbsp;&nbsp;&nbsp;        
                                   <a title="Visualizar el archivo del boletín" class="btn btn-primary" href="{{ url('externo', $boletin->id) }}"><i class="fa fa-eye" aria-hidden="true"></i></a>                                                                    
                                   
                                   @if ($boletin->fecha_boletin >= '2022-08-22')
                                      <a  title="Descargar el archivo del boletín" class="btn btn-success" href="{{$boletin->ruta }}" download="Boletindel{{$boletin->fecha_boletin }}"><i class="fa fa-folder-open" aria-hidden="true"></i></a>
                                   @else 
                                      <a  title="Descargar el archivo del boletín" class="btn btn-success" href="/pdf/boletines/{{$boletin->ruta }}" download="Boletindel{{$boletin->fecha_boletin }}"><i class="fa fa-folder-open" aria-hidden="true"></i></a>
                                   @endif                                  
                                   <!--  <a title="Descargar el archivo del boletín" class="btn btn-success" href="{{  url('/consultaboletinj/descargarboletin', $boletin->id) }}"><i class="fa fa-folder-open" aria-hidden="true"></i></a>                                      
                                   <button onclick="imprimir();" class="btn btn-success" type="submit"><i class="fa fa-folder-open" aria-hidden="true"></i></button> 

                                       
                                         <a title="Visualizar el archivo del boletín" class="btn btn-primary" href="{{ url('interno', $boletin->id) }}"><i class="fa fa-eye" aria-hidden="true"></i></a>                                                                    
                                          
                                        buenas las dos primeras
                                              <button title="Visualizar el archivo del boletín" type="button" class="btn btn-primary" data-toggle="modal" data-target="#boletininterno{{$boletin->id}}"><i class="fa fa-eye" aria-hidden="true"></i></button>                                  
                                            <a  title="Descargar el archivo del boletín" class="btn btn-success" href="http://127.0.0.1:8000/pdf/boletines/{{$boletin->ruta }}" download="Boletindel{{$boletin->fecha_boletin }}">Descarga Boletin</a>

                                        <a class="dropdown-item" href="{{ route('downfile',  $boletin->id) }}">
                                             <span><i class="fa fa-info-circle fa-lg" aria-hidden="true"></i></span> Bajar archivo
                                        </a>-->                                   
                                        <!--
                                        <a  title="Descargar el archivo del boletín" type="button" class="btn btn-primary" href=""><i class="fa fa-eye" aria-hidden="true"></i></a>                                                                         
                                        <a  title="Descargar el archivo del boletín" class="btn btn-success" href="{{  url('/consultaboletinj/descargarboletin', $boletin->id) }}"><i class="fa fa-folder-open" aria-hidden="true"></a> 
                                        <a class="dropdown-item" href="{{ url('/boletinbaja/') }}"> <span><i class="fa fa-info-circle fa-lg" aria-hidden="true"></i></span>Bajar </a> 
                                        <a class="dropdown-item" href="{{ url('/file-download/') }}">  <span><i class="fa fa-info-circle fa-lg" aria-hidden="true"></i></span>Bajar </a>  --> 
                                      </td>									
                              </tr>  
                                    
                               
                                   <!--Ventana Modal para Actualizar--->                        
                                 @include('consulta.boletininterno')                                   
	                        @endforeach
	                     </tbody> 
                       <tfoot>
	              	         <tr style="border: 1px solid"> 
                              <th class="text-center" >No.</th>  						                 
                              <th class="text-center" >Fecha</th>                                                 
                              <th class="text-center" >Acciones</th>      
	                          </tr>
	                      </tfoot>        
                </table>
            </div>   
       </div>
  </div>
</form>

  
@endsection
